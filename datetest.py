import sqlite3
from datetime import datetime

db = sqlite3.connect(':memory:')
c = db.cursor()
c.execute('''create table example (id integer primary key,createdat date)''')
c.execute('''insert into example (createdat) values(?)''',(datetime.now(),))
db.commit()
c.execute('''select * from example''')
print c.fetchall()
db.close()

# '''pythoncentral.io/advanced-sqlite-usage-in-python'''