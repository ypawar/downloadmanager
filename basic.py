import os.path as path
import xmlrpclib
import time
from pprint import pprint
s = xmlrpclib.ServerProxy('http://192.168.0.107:6800/rpc',verbose=False)
# s = xmlrpclib.ServerProxy('http://localhost:6800/rpc')
# r = s.aria2.getFiles('9e9f9245a2c56420')
# #pprint (r)
# #print type(r[0])
# print "length " + str(r[0]['length'])
# print "index " + str(r[0]['index'])
# print "completedLength " + str(r[0]['completedLength'])
# print "path " + str(r[0]['path'])
# print "selected " + str(r[0]['selected'])
# print "index" + str(r[0]['uris'][6])
#
# print type(r)
#
# r = s.aria2.tellStatus('9e9f9245a2c56420', ['gid', 'totalLength', 'completedLength'])
# print r

def hrSize(num, suffix='B'):
    for unit in ['','K','M','G','T','P','E','Z']:
        if abs(num) < 1024.0:
            return "%3.2f %s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Y', suffix)

# print sizeof_fmt(1458176)

def getVersion(server):
    r = server.aria2.getVersion()
    print r['version']
    for f in r['enabledFeatures']:
        print f

def addUri(server,uri,paused='',login='',password=''):
    # if login!='':
    #     if paused==1:
    #         return server.aria2.addUri([uri],{'pause':'true','login':login,'password':password})
    #     else :
    #         return server.aria2.addUri([uri],{'pause':'false'})
    return server.aria2.addUri([uri])

def getOption(server,gid):
    r = server.aria2.getOption(gid)
    pprint(r)

def setOption(server,gid,option,value):
    r = server.aria2.changeOption(gid,{option:value})

def pause(server,gid):
    server.aria2.pause(gid)

def pauseAll(server):
    server.aria2.forcePauseAll()

def resume(server,gid):
    server.aria2.unpause(gid)

def resumeAll(server):
    r = server.aria2.unpauseAll()
    return r

def tellActive(server):
    r = server.aria2.tellActive()
    return r

def tellWaiting(server):
    return server.aria2.tellWaiting(0,1000)

def getFiles(server,gid):
    r = server.aria2.getFiles(gid)
    print r[0]['path']

def getName(server,gid):
    r = server.aria2.getFiles(gid)
    name = r[0]['path']
    #
    # if name=='':
    #     name=r[0]['uris'][0]['uri'].split('/')[-1]
    # else :
    #     name=path.basename(r[0]['path'])
    if name != '':
        name=path.basename(r[0]['path'])
    return name

def getGlobalStat(server):
    return s.aria2.getGlobalStat()
    r = s.aria2.getGlobalStat()
    d,u = hrSize(int(r['downloadSpeed'])),hrSize(int(r['uploadSpeed']))
    return {'d':d,'u':u}

def getGlobalOption(server):
    r = server.aria2.getGlobalOption()
    pprint(r)

def setGlobalOption(server,option,value):
    r = server.aria2.changeGlobalOption({option:value})
    if r =='OK':
        return True
    else :return False

def tellStatus(server,gid):
    r = s.aria2.tellStatus(gid)
    pprint (r)

def listMethods(server):
    r = server.system.listMethods()
    return r
# getVersion(s)
# g = addUri(s,'http://www.flashmagictool.com/download.html&d=FlashMagic.exe')
# pause(s,g)
# tellStatus(s,g)
# pprint(tellWaiting(s))
# pprint(tellActive(s))
# pause(s,'6e5df611945876e0')
# getFiles(s,'6e5df611945876e0')
# print getName(s,'9e9f9245a2c56420')
# resume(s,'9e9f9245a2c56420')
# pprint(getGlobalStat(s))
# time.sleep(5)
# pprint(getGlobalStat(s))
# print getName(s,'9e9f9245a2c56420')

# g = addUri(s,'http://www.partitionwizard.com/download/pwfree91.exe')
# f = addUri(s,'http://downloads.kicad-pcb.org/windows/stable/kicad-product-4.0.4-x86_64.exe')
# print g
# print f
# tellStatus(s,g)
# print getGlobalStat(s)
# time.sleep(5)
# print resumeAll(s)
# time.sleep(3)
getGlobalOption(s)
# setOption(s,'54eb3f579cb0f454','max-connection-per-server','10')
setGlobalOption(s,'max-concurrent-downloads','1')
for i in range(1000):
    print getGlobalStat(s)
    # if i==20:
    #     setGlobalOption(s, 'max-concurrent-downloads', '1')
        # pauseAll(s)
    # pprint(tellActive(s))
    time.sleep(1)
    # getOption(s,'9e9f9245a2c56420')
# pprint(listMethods(s))