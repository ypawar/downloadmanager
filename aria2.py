class aria:
    def __init__(self,server):
        self.globalopt = {'max-connection-per-server': '3', 'dir': '/mnt/yogi/Downloads', 'user-agent': 'aria2/1.18.8',
                 'max-concurrent-downloads': '1'}
        self.server = server
        
    def hrSize(self,num, suffix='B'):
        for unit in ['','K','M','G','T','P','E','Z']:
            if abs(num) < 1024.0:
                return "%3.2f %s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f%s%s" % (num, 'Y', suffix)

    def getVersion(self):
        return self.server.aria2.getVersion()

    def addUri(self,uri,options={}):
        if len(options)==0:
            return self.server.aria2.addUri([uri])
        else:
            return self.server.aria2.addUri([uri],options)

    def getOption(self,gid):
        return self.server.aria2.getOption(gid)
        # pprint(r)

    def setOption(self,gid,option,value):
        return self.server.aria2.changeOption(gid,{option:value})

    def pause(self,gid):
        # return gid of paused download
        return self.server.aria2.pause(gid)

    def pauseAll(self):
        # Returns OK
        return self.server.aria2.forcePauseAll()

    def resume(self,gid):
        # returns gid
        return self.server.aria2.unpause(gid)

    def resumeAll(self):
        # returns OK
        return self.server.aria2.unpauseAll()

    def getActive(self,keys=[]):
        #returns all or specified info using keys of all running downloads
        if len(keys)==0:
            return self.server.aria2.tellActive()
        else:
            return self.server.aria2.tellActive(keys)

    def getWaiting(self,keys=[]):
        return self.server.aria2.tellWaiting(0,1000,keys)

    def getStopped(self,keys=[]):
        if len(keys) == 0:
            self.server.aria2.tellStopped(0, 1000)
        else:
            return self.server.aria2.tellStopped(0,1000,keys)

    def getFiles(self,gid):
        r = self.server.aria2.getFiles(gid)
        path = r[0]['path']
        uri = r[0]['uris'][0]['uri']
        return {'path':path,'uri':uri}

    def getPath(self,gid):
        return self.server.aria2.getFiles(gid)[0]['path']

    def getCompletedLength(self,gid):
        return getFiles(server,gid)[0]['completedLength']

    def getName(self,gid):
        r = getFiles(server,gid)
        name = r[0]['path']
        if name=='':
            name=r[0]['uris'][0]['uri'].split('/')[-1].split['?'][0]
        return name

    def getGlobalStat(self):
        r = self.server.aria2.getGlobalStat()
        d,u = self.hrSize(int(r['downloadSpeed'])),self.hrSize(int(r['uploadSpeed']))
        return {'d':d,'u':u}

    def getGlobalOption(self):
        return self.server.aria2.getGlobalOption()

    def setGlobalOption(self,option,value):
        return self.server.aria2.changeGlobalOption({option:value})

    def getStatus(self,gid,keys=[]):
        if len(keys)==0:
            return self.server.aria2.tellStatus(gid)
        else:
            return self.server.aria2.tellStatus(gid,keys)

    def listMethods(self):
        return self.server.system.listMethods()
