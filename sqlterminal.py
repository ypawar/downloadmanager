import sqlite3 as db

# db = sqlite3.connect('app.db')
ddb = db.connect('app.db',check_same_thread=False)
db = ddb.cursor()

print "SQLite 3 simple shell from python.wikia.com\n\n"

while True:
    cmd = input("sql> ")
    try:
        db.execute(cmd.strip())
        if cmd.lstrip().upper().startswith("SELECT"):
            print db.fetchall()
    except sqlite3.Error, e:
        print "An error occurred:", e.args[0]