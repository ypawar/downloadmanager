import xmlrpclib
import time
from pprint import pprint
import aria2
from flask import Flask,render_template,request
import json
s = xmlrpclib.ServerProxy('http://192.168.0.107:6800/rpc',verbose=False)
from pprint import pprint
# import sqlite3 as db
from datetime import  datetime


# pprint(aria2.getVersion(s)['version'])
# # g = aria2.addUri(s,'http://downloads.kicad-pcb.org/windows/stable/kicad-product-4.0.4-x86_64.exe',{'pause':'true'})
# # print aria2.resume(s,'f2b7cb18314485ab')
# time.sleep(5)
# pprint(aria2.getGlobalStat(s))
# pprint(aria2.getActive(s,['gid']))
# g = aria2.getActive(s,['gid'])[0]['gid']
# # g = aria2.getWaiting(s,['gid'])[0]['gid']
# # print type(g)
# # print g
# print aria2.getStatus(s,g,['status','totalLength'])
# print aria2.getCompletedLength(s,g)
# print aria2.getFiles(s,g)
aria = ''
ddb=''
cursor = ''
app=Flask(__name__)

@app.route("/")
def home():
    # return "Download Manager Home Page"
    return render_template('index2.html')

@app.route('/adduri',methods = ['POST', 'GET'])
def adduri():
    if request.method == 'GET':
        if len(request.args)==0:
            return render_template('add.html')
    else:
        url = request.form['url']
        result = "You provided <br> url: " + str(url)
        g = aria.addUri(url,{'pause':request.form['pause']})
        status = aria.getStatus(g,['status'])['status']
        with ddb:
            cursor.execute('''insert into downloads (gid,uri,addedon,path,status) values(?,?,?,?,?)''',(g,url,datetime.now(),'',status))
        result+= "<br>" + g
        return result

@app.route('/getall',methods=['PSOT','GET'])
def getAll():
    if request.method == 'GET':
        with ddb:
            cursor.execute('''select id,gid,uri,status,path from downloads''')
            r = cursor.fetchall()
        response=[]
        for row in r:
            response.append(
                {
                'id':row[0],
                'gid':row[1],
                'uri':row[2],
                'status':row[3],
                'path':row[4]
                }
            )
        return json.dumps(response)

@app.route('/waiting',methods=['POST','GET'])
def waiting():
    if request.method == 'GET':
        result = aria2.getWaiting(s,['completedLength','dir','downloadSpeed','gid','status','totalLength','files'])
        # pprint(result)
        response=[]
        for d in result:
            response.append(
                {'completedLength':d['completedLength'],
                 'dir':d['dir'],
                 'downloadSpeed':d['downloadSpeed'],
                 'gid':d['gid'],
                 'status':d['status'],
                 'totalLength':d['totalLength'],
                 'path':d['files'][0]['path'],
                 'uri':d['files'][0]['uris'][0]['uri']
                }
            )
        # print response
        # print(response, file=sys.stderr)
        return json.dumps(response)

@app.route('/active',methods=['POST','GET'])
def active():
    if request.method == 'GET':
        result = aria2.getActive(s,['completedLength','dir','downloadSpeed','gid','status','totalLength','files'])
        # pprint(result)
        response=[]
        for d in result:
            response.append(
                {'completedLength':d['completedLength'],
                 'dir':d['dir'],
                 'downloadSpeed':d['downloadSpeed'],
                 'gid':d['gid'],
                 'status':d['status'],
                 'totalLength':d['totalLength'],
                 'path':d['files'][0]['path'],
                 'uri':d['files'][0]['uris'][0]['uri']
                }
            )
        # print response
        # print(response, file=sys.stderr)
        return json.dumps(response)

@app.route('/pauseall',methods=['POST','GET'])
def pasueAll():
    if request.method == 'GET':
        return aria2.pauseAll(s)

@app.route('/resumeall',methods=['POST','GET'])
def resumeAll():
    if request.method == 'GET':
        return aria2.resumeAll(s)

@app.route('/resume',methods=['POST','GET'])
def resume():
    if request.method == 'GET':
        ig=request.args.get('gid')
        response = aria2.resume(s,ig)
        state = aria2.getStatus(s,response,['status'])['status']
        print state
        print type(state)
        if state=='active':state=1
        elif state=='paused':state=3
        elif state=='waiting':state=2
        elif state=='error':state=4
        elif state=='completed':state=5
        if response==ig:
            cursor.execute('''update downloads set state=? where gid=?''',(state,response))

        return response

@app.route('/globalstat',methods=['POST','GET'])
def getGlobalStat():
    if request.method == 'GET':
        response = json.dumps(aria.getGlobalStat())
        print response
        return response
# app.run()