import threading
import time
import downloadApp as da
import sqlite3 as db
import aria2
import xmlrpclib
from os.path import basename
from datetime import  datetime

ddb = db.connect('app.db',check_same_thread=False)
cursor = ddb.cursor()
aria = aria2.aria(xmlrpclib.ServerProxy('http://192.168.0.107:6800/rpc',verbose=False))
initrun=True
ariaLock = threading.Lock()
class server (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        da.aria = aria
        da.cursor = cursor
        da.ddb = ddb
    def run(self):
        da.app.run()

class dmapp (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        while 1:
            # update()
            global initrun
            if initrun==True:
                syncdb()
                initrun=False
            fixstatic()
            checkState()
            time.sleep(5)



def update():
    # cursor.execute(
    #     '''select gid from downloads where (state in ('active','paused','queued') or state is null) and (path is null or path="") ''')
    cursor.execute('''select gid,uri,status from downloads where status in ('active','paused','queued') order by status''')
    g = cursor.fetchall()
    print g
    # print type(g)
    for row in g:
        # print type(r)
        # print r
        gid = row[0]
        uri=row[1]
        with ariaLock:
            try:
                status = aria.getStatus(gid, ['status', 'completedLength', 'downloadSpeed', 'totalLength'])
                path = aria.getPath(gid)
            except e:
                ari

        with ddb:
            cursor.execute('''update downloads set state=?,completed=?,speed=?,size=?,path=? where gid=?''', (
            status['status'], status['completedLength'], status['downloadSpeed'], status['totalLength'], path,gid))


def syncdb():
    #aria active
    aa = aria.getActive(['gid'])
    #aria waiting/paused
    aw = aria.getWaiting(['gid','status'])
    #db active
    cursor.execute('''select gid,uri from downloads where status=?''',("active",))
    dba = cursor.fetchall()
    #db waiting
    cursor.execute('''select gid,uri,status from downloads where status in (?,?)''', ("paused", "waiting"))
    dbw = cursor.fetchall()
    print "Aria active"
    print aa
    print "DB active"
    print dba
    print "Aria paused/waiting"
    print aw
    print "DB paused/waiting"
    print dbw

    #aria just started and first run so sync active/paused downloads
    if len(aa)==0 and len(dba)>0:
        for row in dba:
            try:
                aria.addUri(row[1],{'gid':row[0]})
            except xmlrpclib.Fault as err:
                #If the state of the download is changed then update it in the db
                if (err.faultString == 'No URI to download.'):
                    status = aria.getStatus(row[0],['status'])['status']
                    cursor.execute('update downloads set status=? where gid=?',(status,row[0]))
    if len(aw)==0 and len(dbw)>0:
        for row in dbw:
            aria.addUri(row[1],{'gid':row[0],'pause':'true'})


def fixstatic():
    #Get gid for which path has not been set
    cursor.execute(
        '''select gid from downloads where path=? and status in (?,?,?) order by status asc ''',("",'active','paused','waiting'))
    g = cursor.fetchall()
    for row in g:
        path = aria.getPath(row[0])
        #put values in db if valid path is provided by aria
        if len(path)>1:
            length = aria.getStatus(row[0], ['totalLength'])['totalLength']
            with ddb:
                cursor.execute('''update downloads set path=?,totalLength=?,filename=? where gid=?''', (path,length,basename(path),row[0]))
            print cursor.fetchall()

def checkState():
    cursor.execute(
        '''select gid,status from downloads where status in (?,?,?) order by status asc ''',('active', 'paused', 'waiting'))
    g = cursor.fetchall()
    for row in g:
        status = aria.getStatus(row[0],['status'])['status']
        if status!=row[1]:
            if status=='complete':
                with ddb:
                    cursor.execute('''update downloads set status=?,completedon=? where gid=?''',(status,datetime.now(),row[0]))
            else :
                with ddb:
                    cursor.execute('''update downloads set status=?where gid=?''',(status,row[0]))

def getall():
    with ddb:
        cursor.execute('''update downloads set status=?where id=1''', ('complete',))
        cursor.execute('select * from downloads')
    print cursor.fetchall()

flaskserever = server()
flaskserever.start()

app = dmapp()
app.start()

flaskserever.join()
app.join()

# update()
# getall()
# syncdb()